package ru.tfs.jdbc.batch;

import static ru.tfs.jdbc.Constants.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

public class BatchInsertExample {

    public static void main(String[] args) {

        String sql = "insert into BATCH (id, external_id, internal_id) values(?, ?, ?)";

        try (
            Connection connection = DriverManager.getConnection(URL + DATABASE, USER, PASSWORD);
            PreparedStatement statement = connection.prepareStatement(sql);
            PreparedStatement delete = connection.prepareStatement("delete from BATCH");
        ) {
            delete.executeUpdate();

            // add 10000 rows directly
            LocalDateTime before = LocalDateTime.now();
            for (int i = 0; i < 10000; i++) {
                statement.setObject(1, UUID.randomUUID());
                statement.setObject(2, UUID.randomUUID());
                statement.setObject(3, UUID.randomUUID());
                statement.executeUpdate();
            }
            LocalDateTime after = LocalDateTime.now();
            long between = ChronoUnit.MILLIS.between(before, after);
            System.out.println("direct - " + between);

            delete.executeUpdate();

            // add 10000 rows batch
            LocalDateTime batchBefore = LocalDateTime.now();
            statement.clearBatch();
            for (int i = 0; i < 10000; i++) {
                statement.setObject(1, UUID.randomUUID());
                statement.setObject(2, UUID.randomUUID());
                statement.setObject(3, UUID.randomUUID());
                statement.addBatch();
            }
            statement.executeBatch();
            LocalDateTime batchAfter = LocalDateTime.now();
            between = ChronoUnit.MILLIS.between(batchBefore, batchAfter);
            System.out.println("batch - " + between);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
