package ru.tfs.jdbc.patterns.model;

public class DepartmentVO {
    private Long id;
    private String name;
    private EmployeeVO head;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmployeeVO getHead() {
        return head;
    }

    public void setHead(EmployeeVO head) {
        this.head = head;
    }
}
