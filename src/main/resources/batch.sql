create table public.batch
(
	id uuid not null
		constraint batch_pk
			primary key,
	external_id uuid not null,
	internal_id uuid not null
);

create index batch_external_index
	on public.batch (external_id);

create index batch_internal_index
	on public.batch (internal_id);

